import { useState, useEffect } from "react";

const useFetchMovies = (apiLink) => {
    const [ isLoaded, setLoaded ] = useState(false);
    const [ error, setError ] = useState(null);
    const [ data, setData ] = useState([]);
    const [ totalPages, setTotalPages ] = useState(0);

    const fetchMovies = (apiLink) => {
        fetch(apiLink) 
        .then((response) => response.json())
        .then((result) => {
                setLoaded(true);
                setData(result.results);
                setTotalPages(result.total_pages);
        })
        .catch((err) => {
                setLoaded(true);
                setError(err);
        })
    }

    useEffect(() => {
        if(apiLink) {
            fetchMovies(apiLink);
        }
    })
    return { isLoaded, error, data, totalPages };
}

export default useFetchMovies;