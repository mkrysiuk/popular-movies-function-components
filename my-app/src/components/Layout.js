import React from "react";
import { NavLink, Outlet } from "react-router-dom";

const Layout = () => {
    return (
        <div className="wrapper">
            <div className="header_container">
                <header className="header">
                    <div className="nav_bar">
                        <div className="header_logo">
                            <NavLink to="/"><h3>TheMovieDB</h3></NavLink>
                        </div>
                        <div className="nav-link">
                            <NavLink to="/" className="link">Favourite Movies</NavLink>
                            <NavLink to="/top-rate" className="link">Top Rate Movies</NavLink>
                            <NavLink to="/tv-shows" className="link">TV Shows</NavLink>
                        </div>
                    </div>
                </header>
            </div>
            <main className="main_container">
                <Outlet />
            </main>
            <footer className="main_footer_container">
                <div className="footer_container">
                    <div className="footer_content">
                        <span className="footer_text">&copy; by mkrysiuk 2022</span>
                    </div>
                </div>
            </footer>
        </div>
    );
}

export default Layout;