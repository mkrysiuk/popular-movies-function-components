import React, { useState } from 'react';
import PopUp from './PopUp';

const Image = (props) => {
    const [ showPopUp, setShowPopUp ] = useState(false);
    const imgPath = 'https://image.tmdb.org/t/p/w300';
    
    const togglePopUp = () => {
        setShowPopUp(!showPopUp);
    }

    return (
        <>
            <img 
                onClick={togglePopUp.bind(this)}
                className = "list_img" 
                src = {imgPath + props.imgUrl} 
                alt = {props.alt} 
            />
            {showPopUp ? 
                <PopUp
                    releaseDate = {props.releaseDate}
                    closePopUp = {togglePopUp.bind(this)}
                />
                : null
            }
        </> 
    );
}

export default Image;
