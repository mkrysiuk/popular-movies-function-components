import React, { useState } from 'react';
import AppItem from './AppItem';
import AppWrapper from './AppWrapper';
import ThemeContext, { themes } from './ThemeContext';
import useFetchMovies from "../hooks/useFetchMovies";

const TopRatedMoviesPage = () => {
    const [ currentPage, setCurrentPage ] = useState(1);
    const [ theme, setTheme ] = useState(themes.dark);
    const [ changeTheme, setChangeTheme ] = useState(themes.dark);
 
    const apiKey = 'api_key=bef43a35f0369e6532e4ef01b678f3c9';
    const apiLink = `https://api.themoviedb.org/3/movie/top_rated?${apiKey}&language=en-US&page=${currentPage}`;
    const { totalPages, data, isLoaded, error } = useFetchMovies(apiLink);

    const handleChangePage = (currentPage) => {
        setCurrentPage(currentPage)
    };

    const toggleTheme = () => {
        theme === themes.dark 
        ? setTheme(themes.light)
        : setTheme(themes.dark);

        setChangeTheme(!changeTheme);
    }

    if(error) {
        return <p>Error {error.message}</p>
    } else if (!isLoaded) {
        return (
            <div className = "container_loader">
                <span className = "loader">Loading... </span>
            </div>
        );
    } else {
        return (
            <ThemeContext.Provider value={theme}>
                <AppWrapper 
                    title = "Top Rated Movies"
                    children = {
                        data.map(item => (
                            <AppItem 
                                key = {item.id}
                                title = {item.original_title}
                                popularity = {item.vote_average}
                                imgUrl = {item.poster_path}
                                alt = {item.original_title}
                                releaseDate = {item.release_date}
                                overview = {item.overview}
                                changeTheme = {changeTheme}
                            />
                        ))
                    }
                    currentPage = {currentPage}
                    totalPages = {totalPages}
                    handleChangePage = {handleChangePage}
                    toggleTheme = {toggleTheme}
                    changeTheme = {changeTheme}
                />
            </ThemeContext.Provider>
        );
    }
}

export default TopRatedMoviesPage;