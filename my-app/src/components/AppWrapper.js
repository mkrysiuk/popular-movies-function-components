import React, { useContext } from 'react';
import Pagination from './Pagintion';
import ThemeButton from './ThemeButton';
import ThemeContext from "./ThemeContext";

const AppWrapper = (props) => {
    const theme = useContext(ThemeContext);

    return (
        <div style = {theme}>
            <div className = {props.changeTheme ? "container-dark" : "container-light"}>
                <div>
                    <h1 className = {props.changeTheme ? "main_title-dark" : "main_title-light"}>
                        {props.title}
                    </h1>
                    <ThemeButton 
                        toggleTheme = {props.toggleTheme} 
                        changeTheme = {props.changeTheme}
                    />
                </div>
                <ul className = "render_list">{props.children}</ul>
                <Pagination 
                    currentPage = {props.currentPage}
                    totalPages = {props.totalPages}
                    handleChangePage = {props.handleChangePage}
                    changeTheme = {props.changeTheme}
                />
            </div>
        </div>
    );
}

export default AppWrapper;