import React from 'react';

const Pagination = (props) => {
    return (
        <div className = "pagination">
            <button 
                disabled = {props.currentPage === 1}  
                onClick = {() => props.handleChangePage(props.currentPage - 1)}
                className = {props.changeTheme ? "pagination_btn-dark" : "pagination_btn-light"}
            >
                Previous Page
            </button> 
            <button 
                disabled = {props.currentPage === props.totalPage} 
                onClick = {() => props.handleChangePage(props.currentPage + 1)} 
                className = {props.changeTheme ? "pagination_btn-dark" : "pagination_btn-light"}
            >
                Next Page
            </button> 
        </div>
    );
}

export default Pagination;