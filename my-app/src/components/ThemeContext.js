import React from "react";

export const themes = {
    dark: {
      background: '#333',
      color: '#fff',
    },
    light: {
        background: '#fff',
        color: '#000',
      }
  };

const ThemeContext = React.createContext(themes.dark);
export default ThemeContext;
