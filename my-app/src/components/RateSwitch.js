import React, { useState } from 'react';

const RateSwitch = (props) => {
    const [ showRate, setShowRate ] = useState(false);
    const popularityStr = 'Popularity: ';

    const handleShowRate = () => {
        setShowRate(!showRate);
    }

    return (
        <div className = "container_popularity"> 
            <button 
                className = {props.changeTheme ? "button_rate-dark" : "button_rate-light"} 
                onClick = {handleShowRate}
            >
                {showRate ? 'Hide Rate' : 'Show Rate'}
            </button> 
            {showRate && 
                <p className = "popularity">
                    <span className = {props.changeTheme ? "popularity_rate-dark" : "popularity_rate-light"}>
                        {popularityStr + props.popularity}
                    </span>  
                </p> 
            }
        </div>
    );
}

export default RateSwitch;