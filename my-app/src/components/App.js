import React from "react";
import { Routes, Route } from "react-router-dom";
import FavouriteMoviesPage from "./FavouriteMoviesPage";
import TopRatedMoviesPage from "./TopRatedMoviesPage";
import TVShowsPage from "./TVShowsPage";
import Layout from "./Layout";

const App = () => {
    return (
        <>
            <Routes>
                <Route path="/" element={<Layout />}>
                    <Route index element={<FavouriteMoviesPage />} />
                    <Route path="top-rate" element={<TopRatedMoviesPage />} />
                    <Route path="tv-shows" element={<TVShowsPage />} />
                </Route>
            </Routes>
        </>
    );
}

export default App;