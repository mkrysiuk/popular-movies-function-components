import React from 'react';
import Image from './Image';
import RateSwitch from './RateSwitch';

const AppItem = (props) => {
    return (
        <li className = {props.changeTheme ? "list-dark" : "list-light"}>
            <h3 className = {props.changeTheme ? "title_list-dark" : "title_list-light"}>
                {props.title}
            </h3>
            <RateSwitch 
                popularity = {props.popularity} 
                changeTheme = {props.changeTheme}
            />
            <Image 
                imgUrl = {props.imgUrl} 
                alt = {props.alt} 
                releaseDate = {props.releaseDate}
            />
            <p className = {props.changeTheme ? "list_overview-dark" : "list_overview-light"}>
                {props.overview}
            </p>
        </li>
    );
}

export default AppItem;