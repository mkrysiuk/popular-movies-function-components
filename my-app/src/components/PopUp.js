import React from "react";

const PopUp = (props) => {
    return (
        <div className="popup">
            <div className="popup_inner">
                <button onClick={props.closePopUp} className="popup_close-btn">+</button>
                <h3 className="popup_title">Release Date: {props.releaseDate}</h3>
            </div>
        </div>  
    );
}

export default PopUp;
